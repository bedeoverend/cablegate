<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * This is just gonna run the default seeder, cables
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		$this->call('CableSeeder');

	}

}
