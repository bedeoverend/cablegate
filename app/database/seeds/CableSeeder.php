<?php

class CableSeeder extends Seeder {
	
	public $small = false;

	/**
	 * Run the database seeds.
	 * Rebuild schema (if necessary) and seed with CSV file
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		// Check if it exists and if --force has been given
		if(Schema::hasTable('cables') && !$this->command->option('force')) {
			$this->command->error('cables table already exists. Use --force to overwrite.');
			exit(1);
		}

		// Want to erase the current one if need be
		Schema::dropIfExists('cables');

		// Create the cables table
		Schema::create('cables',function($table) {
			
			// Raw default values in the CSV
			$table->mediumInteger('id')->unsigned();
			$table->primary('id');
			$table->string('date',16);
			$table->string('cable_id', 35);			
			$table->string('from',33);			
			$table->string('classification',35);
			$table->string('references',1896);
			$table->string('zulu',4082);
			$table->mediumText('content');

			// Containers for values yet to be set
			$table->datetime('sent');
			$table->double('neutral');
			$table->double('positive');
			$table->double('negative');
			$table->text('json_cloud');
		});

		// Make content completely searchable
		DB::statement('ALTER TABLE cables ADD FULLTEXT search(content)');
		
		// Talk to the user
		$this->command->info('Cables table created!');

		// Seed the cables table with CSV file
		$this->seed();

	}

	/**
	 * Fills the cables table with raw data from CSV file
	 * @param  boolean $small Should it load the small value or entire cable value
	 * @return void
	 */
	private function seed($small = null) {
		if($small == null)
			$small = $this->small;

		// Tell the user what's going on
		$this->command->info('Seeding '.($small ? 'medium' : 'large').' CSV...');
		
		// Set up query for mass data loading
		$query = "LOAD DATA LOCAL INFILE '" . storage_path() . "/csv/".($small ? "medium" : "large" ) . ".csv' INTO TABLE cables FIELDS TERMINATED BY ',' ENCLOSED BY '\\\"' ESCAPED BY '\\\\' LINES TERMINATED BY '\\n'";

		// Laravel currently can't handle this kind of statement, so have to use the PDO from PHP
		// This will take a long time, it has to take over 250k documents and perform full text indexing on all of them
		$worked = DB::connection()->getpdo()->exec($query);
		
		// Report back to user
		$this->command->info('Seeding of '.($small ? 'medium' : 'large').' CSV '.($worked ? 'successful!' : 'failed!'));
		
		// Get the time taken to execute from the SQL log 
		$time = array_pop(DB::getQueryLog())['time'] / 1000;

		// Report back to user
		$this->command->info(sprintf('Seeded in %.2f seconds',$time));			
	}
}
