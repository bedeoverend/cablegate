<?php

class EmbassySeeder extends Seeder {

	/**
	 * Start seeding the embassies.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		// Make sure we're not overriding the emabssies accidently
		if(Schema::hasTable('embassies') && !$this->command->option('force')) {
			$this->command->error('embassies table already exists. Use --force to overwrite.');
			exit(1);
		}

		// First, rebuild the database table structure
		self::rebuildSchema();

		// Then seed the database
		self::seed();
	}

	/**
	 * Rebuilds the embassies table
	 * @return void
	 */
	public static function rebuildSchema() {
		Schema::dropIfExists('embassies');
		Schema::create('embassies',function($table) {
			$table->increments('id');
			$table->string('name');
			$table->string('address');
			
			$table->double('latitude',10,8);
			$table->double('longitude',11,8);
		});
	}

	/**
	 * Seed all the embassy table with information from the CSV file
	 * @return void
	 */
	public static function seed() {

		// Setup query
		$query = "LOAD DATA LOCAL INFILE '" . storage_path() . "/csv/embassies.csv' INTO TABLE embassies FIELDS TERMINATED BY ',' ENCLOSED BY '\\\"' ESCAPED BY '\\\\' LINES TERMINATED BY '\\r'  (`name`, `address`)";
		
		// Run query on PDO, laravel can't handle these kind of statements yet
		$worked = DB::connection()->getpdo()->exec($query);

		// Don't bother continuing if it didn't work
		if(!$worked)
			return false;

		// Iterate over the embassies, and initialise its coordinates
		Embassy::chunk(100, function($embassies) {
			foreach($embassies as $embassy) {
				$embassy->setCoordinates();
			}
		});
	}
}
