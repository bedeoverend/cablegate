<?php

class CableControllerTest extends TestCase
{

    /**
     * Tests CableController::rawToDate
     *
     */
    public function testRawToDate()
    {   
        $dates = array(
            "6/29/2001 17:11",  
            "3/8/2006 17:32",   
            "11/9/2009 15:34",  
            "12/20/2005 23:18", 
            "11/13/2007 13:54", 
            "10/5/2006 17:20",  
            "12/18/2006 4:24",  
            "10/12/2009 16:11", 
            "11/30/2007 8:17",  
            "9/22/2008 14:06",
        );
        foreach($dates as $input) {
            $date = DateTime::createFromFormat('m/d/Y H:i',$input);
            $actual = CableController::rawToDate($input);

            $this->assertTrue($date->getTimestamp() === $actual->getTimestamp());
        }
    }

    /**
     * Tests CableController::rawToCloud
     *
     */
    public function testRawToCloud()
    {   
        $content = " subject matter jurisdiction ";
        $content = $content.$content.$content;

        $inputs = array(
            array($content, true, 4),
            array($content, false, 7),
            array($content, true, 0)
        );

        foreach($inputs as $testInput) {
            $input = $testInput[0];
            $json = $testInput[1];
            $max = $testInput[2];
            
            $cloud = CableController::rawToCloud($input, $json, $max);
            if($json) {
                $cloud = json_decode($cloud, true);
                $this->assertTrue(json_last_error() === JSON_ERROR_NONE);
            } 
            foreach($cloud as $word => $count) {
                $this->assertTrue(substr_count ( $input , $word ) === $count);
            }
            $this->assertTrue(count($cloud) <= $max);
            $this->assertTrue(is_array($cloud));
        }
    }
}