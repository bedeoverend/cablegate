<?php

class EmbassyTest extends TestCase
{
    /**
     * Tests Embassy::setCoordinates
     *
     */
    public function testSetCoordinates()
    {

        $address = array(
            array('address' => '5 Lockhart St, Caulfield, VIC 3162, Australia', 'lat' => -37.884568, 'lng' => 145.021468),
            array('address' => 'Monash University, Caulfield East, VIC 3145, Australia', 'lat' => -37.8763311, 'lng' => 145.0444464),
            array('address' => '40 Northcote Ave, Caulfield North, VIC 3161, Australia', 'lat' => -37.877858, 'lng' => 145.027944),
        );
        
        foreach($address as $address) {
            $lat = $address['lat'];
            $lng = $address['lng'];
            $address = $address['address'];

            $embassy = new Embassy();

            $embassy->setCoordinates($address);
            
            $this->assertTrue($embassy->latitude == $lat);
            $this->assertTrue($embassy->longitude == $lng);
        }
    }
}