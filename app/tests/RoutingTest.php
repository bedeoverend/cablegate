<?php

class RoutingTest extends TestCase {

	const TOTAL_CABLES = 251287;
	const TOTAL_PAGES = 2513;

	/**
	 * A basic functional test example.
	 *
	 * @return void
	 */
	public function testHome() {
		$response = $this->call('GET','/');
		$this->isOk();

		$view = $response->original;
		$this->assertViewHas('cables');
	}

	public function testBrowse() {
		$response = $this->call('GET', '/page/'.$this->randomPage());
		$this->isOk();

		$view = $response->original;
		$this->assertViewHas('cables');
	}

	public function testSingle() {
	
		$response = $this->call('GET','/cable/'.$this->randomCable());
		$this->isOk();

		$view = $response->original;
		$this->assertViewHas('cable');
	}

	public function testMap() {
		$response = $this->call('GET','/analyse/map');
		$this->isOk();
	}

	public function testCloud() {
		$response = $this->call('GET','/analyse/cloud');
		$this->isOk();
	}

	public function testSingleCloud() {
		$response = $this->call('GET','/analyse/cloud/'.$this->randomCable());
		$this->isOk();

		$view = $response->original;
		$this->assertViewHas('tags');
	}

	private function isOk() {
		$this->assertResponseOk();
	}

	private function randomCable() {
		return rand(1, self::TOTAL_CABLES);
	}

	private function randomPage() {
		return rand(1, self::TOTAL_PAGES);
	}
}
