<!DOCTYPE html>
<html>
	<head>
		<title>Cablegate Viewer</title>
		<link rel="stylesheet" href="{{ URL::asset('assets/css/style.css') }}" type="text/css" media="all">
		<link href='http://fonts.googleapis.com/css?family=Inconsolata:400,700' rel='stylesheet' type='text/css'>
		<script type="text/javascript" src="{{ URL::asset('assets/js/jquery/jquery.min.js') }}"></script>
		<script type="text/javascript" src="{{ URL::asset('assets/js/jquery/jquery.nouislider.all.min.js') }}"></script>
		<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBx_HLXa5arV5uibQ9COurv09jzXHiWVwU&libraries=visualization"></script>

		{{-- Let a layout add any element to the head --}}
		@yield('head')
	</head>
    <body>
        <header class="main">
			<h1>Cablegate Viewer</h1>
			<nav>
				<a href="{{ URL::route('browse', array('page' => 1)); }}">browse</a> ~ <a href="{{ URL::route('map') }}">map</a> ~ <a href="{{ URL::route('cloud') }}">tag cloud</a>
			</nav>
        </header>
        @yield('content')
        @yield('timeline')
		@yield('footer')
        <script type="text/javascript" src="{{ URL::asset('assets/js/app.js') }}"></script>
        @yield('after_scripts')
    </body>
</html>