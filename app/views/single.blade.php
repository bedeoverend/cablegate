{{-- This layout extends the master layout --}}
@extends('layouts.master')

{{-- Define content section --}}
@section('content')
<div class="container single">
	<article class="cable" id="{{ $cable->id }}">
		<header>
			<h2>{{ $cable->cable_id }} from {{ $embassy->name or 'unknown' }}</h2>
			<h4 class="left classification">{{ $cable->classification }}</h4>
			<h4 class="right">{{ $cable->sent }}</h4>
		</header>
		<pre>
		{{ $cable->content }}
		</pre>
		<footer>
			<ul>
				<li><a href="{{ URL::route('cloud', array('cloud' => $cable->id)) }}">tag cloud</a></li>
			</ul>
		</footer>
	</article>
</div>
@stop

{{-- Define footer --}}
@section('footer')
<footer class="main">
cablegate - made by <a target="_blank" href="http://bedeoverend.com">bede.</a>
</footer>
@stop