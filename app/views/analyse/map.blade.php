<?php
/* 
* @Author: Bede
* @Date:   2014-10-01 17:54:42
* @Last Modified by:   Bede
* @Last Modified time: 2014-10-04 18:48:18
*/
?>
@extends('layouts.master')

@section('head')

@stop

@section('content')
<div class="container map">
	<div id="map-canvas"></div>
</div>`
@stop

@section('timeline')
<div id="timeline" class="disabled">
	<span id="min-year">1966</span>
	<div id="timeline-control"></div>
	<span id="max-year">2010</span>
</div>
@stop