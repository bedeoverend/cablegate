<?php
/* 
* @Author: Bede
* @Date:   2014-10-01 17:54:42
* @Last Modified by:   Bede
* @Last Modified time: 2014-10-01 18:00:09
*/
?>
@extends('layouts.master')

@section('head')
<script type="text/javascript" src="{{ URL::asset('assets/js/jquery/jquery.tagcanvas.js') }}"></script>
@stop

@section('content')

<div class="container cloud {{ $type }}">
	<div id="cloud">
		<canvas id="cloudCanvas" class="cloud">
			@if($tags)
			<ul>
				@foreach($tags as $name => $value) 
				<li><a data-count="{{ $value }}" href="{{ URL::route('browse', array('page' => 1,'search' => $name)) }}">{{ $name }}</a></li>
				@endforeach
			</ul>
			@endif
		</canvas>
	</div>
</div>

@stop

@section('timeline')
@if($type !== 'single')
<div id="timeline" class="disabled">
	<span id="min-year">1966</span>
	<div id="timeline-control"></div>
	<span id="max-year">2010</span>
</div>
@endif
@stop