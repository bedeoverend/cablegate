{{-- Extends the master layout --}}
@extends('layouts.master')

@section('content')
{{ Form::open(array( 'url' => URL::route('browse', array('page' => 1)), 'method' => 'get')) }}
{{ Form::label('search') }}{{ Form::text('search', Input::get('search')); }}
{{ Form::close() }}
<div class="container browse">
  <table class="table-borders">
    <thead>
      <tr>
        <th>ID @if('sort' == 'cable_id')<span class="{{ $dir }}"></span> @endif</th>
        <th>Timestamp @if('sort' == 'sent')<span class="{{ $dir }}"></span> @endif</th>
        <th>Embassy @if('sort' == 'sent')<span class="{{ $dir }}"></span> @endif</th>
        <th>Classification @if('sort' == 'sent')<span class="{{ $dir }}"></span> @endif</th>
      </tr>
    </thead>
    <tbody>
    	@foreach($cables as $cable)
      <tr id="{{ $cable->id }}" data-link="{{ URL::route('single',array('cable' => $cable->id)) }}">
        <td>{{{ $cable->cable_id }}}</td>
        <td>{{{ $cable->sent }}}</td>
        <td>{{{ $cable->embassy->name or "Unknown" }}}</td>
        <td>{{{ strtoupper($cable->classification) }}}</td>
      </tr>
      @endforeach
    </tbody>
  </table>
</div>
<nav class="pagination">
  @foreach($pages as $page)
  <a @if($page['current']) class="current" @endif href="{{ $page['link'] }}">{{ $page['number'] }}</a>
  @endforeach
</nav>
@stop

@section('footer')
<footer class="main">
cablegate - made by <a target="_blank" href="http://bedeoverend.com">bede.</a>
</footer>
@stop