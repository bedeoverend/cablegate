<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

/**
 * Artisan command for generating the tag cloud information for the cables
 */
class GenerateTags extends Generate {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'generate:tags';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Generate and save tag clouds on all cables.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{

		// Inherits from Generate, so has access to iterate, 
		// Pass it the closure function with cable as a reference parameter
		$this->iterate(function(&$cable) {
			
			// Convert content to json_data representing word frequencies
	    	$cable->json_cloud = CableController::rawToCloud($cable->content);
	    	
		});
	}
}
