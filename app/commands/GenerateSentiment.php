<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

/**
 * Artisan command for generating the sentiment information for the cables
 */
class GenerateSentiment extends Generate {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'generate:sentiment';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Generate and save sentiment analysis on all cables.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		// Inherits from Generate, so has access to iterate, 
		// Pass it the closure function with cable as a reference parameter
		$this->iterate(function(&$cable) {
			// Run Sentiment Analysis on the cables content, then update the cable.
	    	$sentiments = SentimentAnalysis::scores($cable->content);
	    	$cable->negative = $sentiments['negative'];
	    	$cable->neutral = $sentiments['neutral'];
	    	$cable->positive = $sentiments['positive'];
		});
	}
}
