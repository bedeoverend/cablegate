<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

/**
 * Artisan command for caching the tag cloud data
 */
class CacheCloud extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'cache:cloud';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Refresh the cache for the cloud tag data.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		// Notify user
		$this->info('Caching...');

		// Set min and max years
		$start = 1966;
		$end = 2010;

		// Timing functions for reporting to user
		$total = $end - $start;
		
		$base_time = microtime(true);
		$cumul_rate = 0;
		$done = 0;
		for($i = $start; $i <= $end; $i++) {
			
			// Cache particular year
			CloudController::cacheYear($i);

			// Calculate time details and notify user
			$total_time = (microtime(true) - $base_time);
		   
		    $rate = (++$done / $total_time); 
		    $remaining = $total - $done;
		    $est = $rate == 0 ? -1 : $remaining / $rate / 60;
		    $perc = ($done / $total)*100;
		    
		    $this->getOutput()->write(sprintf("\r<info>$i %.2f%% complete. Speed: %.2f years/s. Est. time remaining: %.2f minutes</info>",$perc,$rate,$est));

		};
		// Notify user that caching is complete
		$this->info('Caching Complete');
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
		);
	}

}
