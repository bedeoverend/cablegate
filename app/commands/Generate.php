<?php

use Illuminate\Console\Command;

/**
 * Abstract Generate class.
 * Should be parent of all classes used to generate data for a given Cable
 */
abstract class Generate extends Command {

	/**
	 * Iterates over all Cable data in a specific chunk.
	 * Given callback function is used performed on the current cable, then that cable is saved.
	 * @param  closure $callback function to perform on current cable
	 * @return void
	 */
	protected function iterate($callback) {
		
		// Notify user
		$this->info('Generating...');
		
		// Total number of cables
		$total = 251287;
		// Start time calculations
		$base_time = microtime(true);
		$cumul_rate = 0;
		// Set the chunking rate i.e. $max number of cables will be loaded for iterating at a time
		$max = 5000;
		$i = 0;

		// Iterate over a chunked section of the cables, this will reduce RAM consumption
		// Pass closure the callback function and all needed variables for time estimations
		Cable::chunk($max,function($cables) use ($callback, $max, $total, $base_time, &$i, &$cumul_rate) {
			
			foreach($cables as $cable) {

				call_user_func_array($callback,array(&$cable));
				$cable->save();

			}

			// Run time calculations, then update the user on what's going on
			$i++;
			$total_time = (microtime(true) - $base_time);
		    $rate = (($i * $max) / $total_time);
		    $cumul_rate = $cumul_rate + $rate;
		    $av_rate = $cumul_rate / $i;
		    $remaining = $total - ($i * $max);
		    $est = $rate == 0 ? -1 : $remaining / $av_rate / 60;
		    $perc = (($i * $max) / $total)*100;
		    
		    $this->getOutput()->write(sprintf("\r<info>%.2f%% complete. Speed: %.2f r/s. Est. time remaining: %.2f minutes</info>",$perc,$rate,$est));

		});
	} 

}