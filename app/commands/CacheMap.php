<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

/**
 * Artisan command for caching the map data
 */
class CacheMap extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'cache:map';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'For the map data to be refreshed and cached.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		// Notify user
		$this->info('Caching map...');
		
		$base_time = microtime(true);

		// Cache particular year
		MapController::cacheJSON();

		// Notify user that caching is complete
		$this->info(sprintf('Caching Complete. Duration: %.2f seconds', microtime(true) - $base_time));
	}
}
