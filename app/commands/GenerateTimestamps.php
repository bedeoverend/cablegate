<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class GenerateTimestamps extends Generate {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'generate:timestamps';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Generate and save timestamp on all cables.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		// Inherits from Generate, so has access to iterate, 
		// Pass it the closure function with cable as a reference parameter
		$this->iterate(function(&$cable) {
			
			// Get the date information as a PHP Datetime object for saving into db
	    	$cable->sent = CableController::rawToDate($cable->date);
	    	
		});
	}
}
