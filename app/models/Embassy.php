<?php

/**
 * Embassy Model
 * Most variable can be accessed through Eloquent interface
 * i.e. embassy data can be accessed by using the column name
 * Eloquent will also implement a Laravel Facade for easy DB row retrieval
 */
class Embassy extends Eloquent {

	// Set what table it will use
	// Necessary here as plural of embassy is embassies, not embassys...
	protected $table = 'embassies';

	// No need for timestamps
	public $timestamps = false;

	/**
	 * Sets the coordinates for the embassy
	 * @param boolean $address If false, uses the stored address, otherwise uses $address (assumes string)
	 */
	public function setCoordinates($address = false) {
		try {
			// Use Geocoder to reverse geocode the address into latitude and longitude
			$geo = Geocoder::geocode( ($address === false ? $this->address : $address) )->toArray();
			$this->latitude = $geo['latitude'];
			$this->longitude = $geo['longitude'];
			
			// Update model to database
			$this->save();
			return true;
		} catch (\Exception $e) {
			Log::error('Exception thrown in Geocoder for embassy '.$this->name);
			return false;
		}
	}

	/**
	 * Define has many relationship with cables
	 * Note that using $this->cables() the relationship object
	 * 		using $this->cables will return a collection of cables belonging to this embassy
	 * @return HasMany Relatationship between cables and embassy
	 */
	public function cables() {

		// 'from' is the foreign key, i.e. where to look for the local key ('name') in the cables table
		return $this->hasMany('Cable','from','name');
	}
}
