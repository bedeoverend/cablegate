<?php

/**
 * Cable model
 * Most information is handled by eloquent
 * 		this means that any data can be referenced by its column value
 */
class Cable extends Eloquent {

	// Doesn't need any timestamps
	public $timestamps = false;

	/**
	 * Each Cable belongsTo and embassy
	 * Using magic methods, calling $this->embassy 
	 * 		will return the Embassy object that this cable was sent from
	 * Note that this function itself will return a belongsTo object, 
	 * 		$this->embassy will get an Embassy object 
	 * @return BelongsTo Relationship between cable and embassy 
	 */
	public function embassy() { 

		// Set the local key to 'from' i.e. the value in the specific row 
		// 		to use as the key in the foreign table (i.e. embassies)
		// 		in this case, 'from' which is the embassy name where the cable came from
		// Set the parent column to 'name' which is the column to look in with the local key
		return $this->belongsTo('Embassy','from','name');
	}

	/**
	 * Returns tag cloud of given object
	 * @param  boolean $json Should a JSON string be returned, or an array?
	 * @return mixed        JSON string or Array, depending on $json
	 */
	public function cloud($json = false) {
		// First check if it should be returned in jason format or not
		$cloud = $json ? $this->json_cloud : json_decode($this->json_cloud, true);

		// Check first to see if the json_cloud exists and is valid
		// If it is, return it, otherwise regenerate the data using CableController::rawToCloud
		return ( $cloud && (!$json && is_array($cloud)) ) ? $cloud : (CableController::rawToCloud($this->content, $json));
	}
}
