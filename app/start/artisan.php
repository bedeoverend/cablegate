<?php

/*
|--------------------------------------------------------------------------
| Register The Artisan Commands
|--------------------------------------------------------------------------
|
| Each available Artisan command must be registered with the console so
| that it is available to be called. We'll register every command so
| the console gets access to each of the command object instances.
|
*/

Artisan::add(new GenerateTags);
Artisan::add(new GenerateSentiment);
Artisan::add(new GenerateTimestamps);
Artisan::add(new CacheCloud);
Artisan::add(new CacheMap);