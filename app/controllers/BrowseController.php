<?php

class BrowseController extends BaseController {

	// Set up constants for paging
	const CABLES_PER_PAGE = 100;
	const LINK_NUM = 10;

	protected $layout = 'browse';

	/**
	 * Browse the current webpage
	 * @param  int 	$page Current page to view
	 * @return View       View object rendered with current layout
	 */
	public function view($page = false) {

		// Fetch any GET params, with defaults
		$order = Input::get('order','id');
		$dir = Input::get('dir','asc');
		$search = Input::get('search',false);
		$page = $page ? $page : Input::get('page',1);
		
		// Select cables as needed
		if($search) {
			// Run full text search if there's a search param
			$base = Cable::select(DB::raw("*, MATCH(content) AGAINST (? IN BOOLEAN MODE) AS score"))
						->whereRaw("MATCH(content) AGAINST (? IN BOOLEAN MODE)")
						->addBinding($search)
						->addBinding($search)
						->orderby('score','desc');
		} else {
			// Otherwise order by the given parameter
			$base = Cable::orderby($order,$dir);
		}

		// Check how many to skip over
		$skip = ($page - 1) * self::CABLES_PER_PAGE;

		// Fetch all the cables
		$cables = $base->skip($skip)->take(self::CABLES_PER_PAGE)->get();

		// Get pagination
		$pages = array(); 
		$pages = $this->paginate($page, $search);

		// Render the layout
		return $this->layout->withCables($cables)->withPages($pages)->withSort($search ? $order : 'score')->withDir($dir);
	}

	/**
	 * Generates an array of page links
	 * @param  int  	$page   Current page number
	 * @param  mixed 	$search Search string, if exists
	 * @return array          	An array of page links
	 */
	private function paginate($page, $search = false) {
	
		$pages = array();
		$numbers = array();

		// If search is present, limit count to full text search results
		if($search) {
			$total = Cable::whereRaw("MATCH(content) AGAINST (? IN BOOLEAN MODE)")
						->addBinding($search)
						->count();

		} else {
			// Faster than count, as the DB is static, can just use the last ID as the count
			$total = Cable::orderby('id','DESC')->take(1)->pluck('id');
		}

		// Get number of the last page
		$last = ceil($total / self::CABLES_PER_PAGE);

		// Helper function for making a page
		function makePage($current = false, $number, $linkTo) {
			 return array(
					'current' => $current,
					'number' => $number, 
					'link' => URL::route('browse', array_merge(Input::all(), array('page' => $linkTo)))
				);
		}

		// Show < and << if necessary
		if($page - 1 > 0) {
			array_push($pages, makePage(false, '&lsaquo;', $page - 1));
		}
		if($page - 10 > 0) {
			array_push($pages, makePage(false, '&laquo;', $page - 10));	
		}

		// Only bother if there are enough pages to create these links
		if($last > self::LINK_NUM) {

			// Always give the first and last two pages
			$start = array(1,2);
			$end = array($last - 1, $last);

			// Get the upper and lower limits of the middle set of links 
			$mid_min = $page - floor(self::LINK_NUM/2);
			$mid_max = $page + floor(self::LINK_NUM/2);

			// Make sure they're not below / above the $start and $end sets
			if($mid_min < 3)
				$mid_min = 3;

			if($mid_max > $last - 2)
				$mid_max = $last - 2;

			// Get all the numbers between
			$mid = range($mid_min, $mid_max);

			// Merge the start, middle and end page numbers
			$numbers = array_merge($start, $mid, $end);

			// Iterate over each page number
			foreach($numbers as $index => $num) {

				// Add the page number onto the pages array
				// convert the page number into a link with useful information
				array_push($pages, makePage($num == $page, $num, $num));

				// If there was a jump between the two numbers, add in a '...' for the links
				if(array_key_exists($index + 1, $numbers) && ($num + 1) != $numbers[$index+1]) {
					array_push($pages,makePage(false, '...', $num));
				}
			}
		} else {
			// There are no gaps, so just make all the pages normally
			for($i = 1; $i <= $last; $i++) {
				array_push($pages, makePage(false,$i, $i));
			}
		}

		// Add >> and > if necessary
		if($page + 1 <= $last) {
			array_push($pages, makePage(false, '&rsaquo;', $page + 1));	
		}
		if($page + 10 <= $last) {
			array_push($pages, makePage(false, '&raquo;', $page + 10));		
		}

		return $pages;
	}
}