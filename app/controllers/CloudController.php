<?php

class CloudController extends BaseController {

	const START = 1966;
	const FINISH = 2010;	

	protected $layout = 'analyse.cloud';

	/**
	 * Render the view connected with the current controller
	 * @return html/text HTML of View
	 */
	public function view(Cable $cable = null) {
		return $this->layout->withTags($cable == null ? null : $cable->cloud())->withType($cable == null ? 'timeline' : 'single');;
	}

	/**
	 * Return an array of popular words broken down by year
	 * @return text/json JSON formatted text, with HTTP Response header
	 */
	public static function fetchJSON() {
		$years = array();
		for($i = self::START; $i <= self::FINISH; $i++) {
			$years[$i] = self::yearCloud($i);
		}
		return Response::json($years);
	}

	/**
	 * Force re-cache of specific year's popular words
	 * @param  int 	$year Year to cache
	 * @return void
	 */
	public static function cacheYear($year) {
		self::yearCloud($year, true);
	}

	/**
	 * Merge two popular word lists, representing tag clouds
	 * Takes key => value pairs from $b and inserts into $a
	 * In the case of a conflict, it will store the higher of the two values
	 * @param  array $a Pointer to first array, will hold the merged array after this
	 * @param  array $b Second array to merge with first
	 * @return array    $a, which holds both 
	 */
	private static function mergeClouds(&$a, $b) {
		// Below is the naive approach, doesn't deal with conflict appropriately
		// $a = array_merge($a, $b);
		
		// Iterate over each key => value in $b
		foreach($b as $word => $count) {
			// Check if it exists in $a
			if(array_key_exists($word, $a)) {
				// Yes, then only put it into $a if the value is larger
				if($count > $a[$word]) 
					$a[$word] = $count;
				
			} else {
				// No, just add it into $a
				$a[$word] = $count;
			}
		}
		
		// Sort the $a array so that the values with the higher count are at the top
		arsort($a);
		
		// We only want the top 15 values
		$a = array_slice($a,0,15);	

		// Returning isn't necessary, but helpful if they want to store it in another value as well
		return $a;
	}

	/**
	 * Generate a frequency table of the top most used words
	 * @param  int  	$year   Year to generate words for
	 * @param  boolean 	$forget Whether to forget the cached version of the tag cloud
	 * @return array          	Array of words most popular words for the year
	 */
	private static function yearCloud($year, $forget = false) {

		// Useful if the database has changed
		if($forget)
			Cache::forget("cloud_$year");

		// Value should be first cached, then returned
		$cloud = Cache::rememberForever("cloud_$year", function() use ($year) {
			
			// Get all cables in the given year
			$cables = Cable::whereRaw("Year(cables.sent) = $year")->get();

			// If there're not cables, don't bother with the rest
			if($cables->isEmpty())
				return array();

			// At every cable pair, merge it into one
			// Use that previous value as the first array in the next merge
			
			$a = null;
			$b = null;
			$first = true;
			foreach($cables as $cable) {
				
				// If its the first cable, initialise the first value as the cable
				if($first) {
					$a = $cable->cloud();
					$first = false;
					continue;
				}
				
				// Fetch the second cable word list, and merge with the current one
				$b = $cable->cloud();
				self::mergeClouds($a, $b);
			}
			return $a;	
		});
		
		return $cloud ? $cloud : array();
	}
}