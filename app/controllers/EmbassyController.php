<?php

class EmbassyController extends BaseController {

	// View has not been made
	// protected $layout = 'embassy';

	/**
	 * Initialise Embassies in database
	 * Simply call prebuilt artisan command
	 * @return mixed    Result of system call
	 */
	public static function initialise($force = false) {
		if(Schema::hasTable('embassies') && (!Input::get('force') && !$force))
			return 'Nah, it already exists, and I dont see a force statement';

		return Artisan::call('db:seed', array('--class' => 'EmbassySeeder'));
	}
}