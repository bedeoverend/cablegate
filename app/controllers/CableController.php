<?php

class CableController extends BaseController {

	protected $layout = 'single';

	/**
	 * Render the view for a single cable
	 * @param  Cable  $cable Cable model to render with
	 * @return View        	View object with rendered HTML
	 */
	public function view(Cable $cable) {
		return $this->layout->withCable($cable)->withEmbassy($cable->embassy);
	}

	/**
	 * Convert raw date information of cable into DateTime object
	 * @param  string $input Timestamp from cable
	 * @return DateTime      DateTime instance of timestamp
	 */
	public static function rawToDate($input) { //: DateTime {
		return DateTime::createFromFormat('m/d/Y H:i',$input);
	}

	/**
	 * Convert the content of a cable into a word frequency table
	 * Only selects the 15 most popular words
	 * @param  string  $input String to analyse
	 * @param  boolean $json  Should the output be a string in JSON format? If no, return Array
	 * @param  integer $max   The maximum number of words to return
	 * @return mixed          Either JSON or Array of words with their corresponding score
	 */
	public static function rawToCloud($input, $json = true, $max = 15) {
		// Retrieve stop words
		$stops = explode("\n", file_get_contents(storage_path().'/meta/stopwords.txt'));
		$output = self::extract_common_words($input, $stops, $max);
		return $json ? json_encode($output) : $output;
	}

	/**
	 * Extract top common words (word cloud)
	 * See second line for addition to remove dashes (possibly could just edit 4th line of function)
	 * From: http://stackoverflow.com/a/7944981
	 * @param  string  			$string     words to build frequency table from
	 * @param  array  			$stop_words words to not include in frequency table
	 * @param  integer 			$max_count  maximum number of words to respond
	 * @return array              			array of words with their corresponding score
	 */
	private static function extract_common_words($string, $stop_words, $max_count = 5) {
		$string = preg_replace('/ss+/i', '', $string);
		$string = preg_replace('/[-]+/i',' ', $string); // Remove dashes, otherwise you can end up with things like '---'
		$string = trim($string); // trim the string
		$string = preg_replace('/[^a-zA-Z -]/', '', $string); // only take alphabet characters, but keep the spaces
		$string = strtolower($string); // make it lowercase

		preg_match_all('/\b.*?\b/i', $string, $match_words);
		$match_words = $match_words[0];
		

		foreach ( $match_words as $key => $item ) {
		  if ( $item == '' || in_array(strtolower($item), $stop_words) || strlen($item) <= 2 ) {
		      unset($match_words[$key]);
		  }
		}  

		$word_count = str_word_count( implode(" ", $match_words) , 1); 
		$frequency = array_count_values($word_count);
		arsort($frequency);

		$keywords = array_slice($frequency, 0, $max_count);
		return $keywords;
	}

	/**
	 * Initialise Cables in database
	 * Simply call prebuilt artisan command
	 * @return mixed    Result of system call
	 */
	public static function initialise($force = false) {
		if(Schema::hasTable('cables') && (!Input::get('force') && !$force))
			return 'Nah, it already exists, and I dont see a force statement';

		return Artisan::call('db:seed', array('--class' => 'CableSeeder'));
	}

	/**
	 * Generate sentiments for cables
	 * Simply call prebuilt artisan command
	 * @return mixed    Result of system call
	 */
	public static function generateSentiments() {
		return Artisan::call('generate:sentiments');
	}

	/**
	 * Generate tag clouds for cables
	 * Simply call prebuilt artisan command
	 * @return mixed    Result of system call
	 */
	public static function generateClouds() {
		return Artisan::call('generate:sentiments');
	}
}