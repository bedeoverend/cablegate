<?php

class MapController extends BaseController {

	protected $layout = 'analyse.map';

	/**
	 * Render the view connected with the current controller
	 * @return View 	View object with current rendered layout
	 */
	public function view() {	
		return $this->layout;
	}

	/**
	 * Generate, cache and return JSON data needed for rendering the heatmap
	 * @param  boolean $forget Should the cache be cleared first?
	 * @return plaintext/json JSON data with http header information
	 */
	public static function fetchJSON($forget = false) {
		
		// Useful if the database has been changed
		if($forget)
			Cache::forget('map_json');

		// Cache the output, the data will never change as the database is static
		$json = Cache::rememberForever('map_json', function() {
			
			/*
			
			SELECT Count(*)            AS 'count', 
			       embassies.name      AS 'name', 
			       embassies.longitude AS 'lng', 
			       embassies.latitude  AS 'lat', 
			       Year(cables.sent)   AS 'year' 
			FROM   cables 
			       INNER JOIN embassies 
			               ON cables.embassy_id = embassies.id 
			GROUP  BY embassies.name, 
			          Year(cables.sent) 
			ORDER  BY Year(cables.sent); 

			 */
			
			// Executes the above SQL code, but in a safer format
			// Fetches embassy information and cables sent grouped by the years 
			// This means that for every year, it will output 
			// 		every embassy and the number of cables sent
			// 		from that embassy, in that year
			$raw = DB::table('cables')
						->join('embassies','cables.from','=','embassies.name')
						->select(
							DB::raw(
								'Count(*)           AS \'count\', 
								embassies.name      AS \'name\', 
								embassies.longitude AS \'lng\', 
								embassies.latitude  AS \'lat\', 
								Year(cables.sent)   AS \'year\''
							)
						)
						->groupBy('embassies.name')
						->groupBy(DB::raw('Year(cables.sent)'))
						->orderBy(DB::raw('Year(cables.sent)'))
						->get();

			// Initialise output array, which will be converted to json
			$output = array();
			
			// Iterate over each row (embassy name with cable count and year)
			foreach($raw as $row) {
				// Check if the output embassy has been made already
				// If not, initialise it with its latitude, longitude and array of years
				if(!array_key_exists($row->name, $output)) {
					$output[$row->name] = array(
							'lng' => $row->lng,
							'lat' => $row->lat,
							'years' => array()
						);
				}

				// Set the number of cables sent for that year at that embassy
				$output[$row->name]['years'][$row->year] = $row->count;

			}
			// Encode the output as a http JSON response and return
			return Response::json($output);
		});
		return $json;
	}

	/**
	 * Force re-cache of JSON map data
	 * @return text/json JSON formatted text, with HTTP Response header
	 */
	public static function cacheJSON() {
		self::fetchJSON(true);
	}
	
}