<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/////////////
// Browse  //
/////////////

Route::get('/', array('uses' => 'BrowseController@view', 'page' => 1));

Route::get('/page/{page}', array('uses' => 'BrowseController@view', 'as' => 'browse'))
->where('page','[1-9][0-9]*');

/////////////
// Single  //
/////////////
Route::model('cable','Cable');
Route::get('/cable/{cable}',array('uses' => 'CableController@view', 'as' => 'single'));

/////////////
// Analyse //
/////////////

Route::get('/analyse/map', array('uses' => 'MapController@view', 'as' => 'map'));
Route::get('analyse/cloud/{cable?}', array('uses' => 'CloudController@view', 'as' => 'cloud'));

///////////
// JSON  //
///////////

Route::get('/json/map', 'MapController@fetchJSON');
Route::get('/json/cloud', 'CloudController@fetchJSON');

/////////////////////
// Administrative  //
/////////////////////

Route::get('/seed/cables', 'CableController@initialise');
Route::get('/seed/embassies', 'EmbassyController@initialise');

