/* 
* @Author: Bede
* @Date:   2014-08-24 15:21:36
* @Last Modified by:   Bede
* @Last Modified time: 2014-10-04 14:09:27
* @Description: Sets up parameters for running grunt, see http://gruntjs.com/
*/
module.exports = function(grunt) {

  // Project configuration.
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		paths: {
			dist: '../../../public/assets',
			src: 'src'
		},
		sass: {                              // Run SASS pre-processing
		    dist: {                          // For distribution  
				options: {                      
					style: 'expanded',
					loadPath: ['bower_components/bitters/app/assets/stylesheets', // Load all the give files
								'bower_components/bourbon/dist',
								'bower_components/neat/app/assets/stylesheets',
								'src/scss']
				},
				files: [{
					expand: true,
					cwd: '<%= paths.src %>/scss',	// Get SCSS source files folder
					src: ['*.scss'],				// Wildcard match against all .scss extensions
					dest: '<%= paths.src %>/css',	// Set output folder (not not distribution, yet to be concatenated)
					ext: '.css'						// Set output extension
				}]
		    }
	  	},
		concat: {									// Concatenate all files
		  dist: {
		    // the files to concatenate
		    src: ['<%= paths.src %>/js/*.js'],
		    // the location of the resulting JS file
		    dest: '<%= paths.dist %>/js/app.js',
		    options: {
		    	// define a string to put between each file in the concatenated output
		    	separator: ';'
		  	}
		  },
		  css: {									// Run css concatentation
		  	src: ['<%= paths.src %>/css/*.css'],	// Input files
		  	dest: '<%= paths.dist %>/css/style.css',// Output file
		  	options: {
		  		separator: ''						
		  	}
		  }
		},
		csso: {										// Compresses and optimizes CSS files
			compress: {
				files: {
					'<%= paths.dist %>/css/style.min.css': '<%= concat.css.dest %>'
				}
			}
		},
		uglify: {									// Minifies JS files
		  options: {
		    // the banner is inserted at the top of the output
		    banner: '/*! <%= pkg.name %> <%= grunt.template.today("dd-mm-yyyy") %> */\n'
		  },
		  dist: {
		    files: {
		      '<%= paths.dist %>/js/app.min.js': ['<%= concat.dist.dest %>']
		    }
		  }	
		},
		jshint: {									// Lint JS files, useful for debugging
		  // define the files to lint
		  files: ['Gruntfile.js', '<%= paths.src %>/js/app.js'],
		  // configure JSHint (documented at http://www.jshint.com/docs/)
		  options: {
		    globals: {
		      jQuery: true,
		      console: true,
		      module: true
		    }
		  }
		},
		stylus: {									// Preprocess stylus files
		  compile: {
		    options: {
		      urlfunc: 'embedurl', // use embedurl('test.png') in code to trigger Data URI embedding
		      use: [
		      	require('jeet') // use stylus plugin at compile time
		      ],
		      compress: false	// No point compressing, CSSO will do that for us
		    },
		    files: {
				'<%= paths.src %>/css/style.css': '<%= paths.src %>/styl/style.styl' // 1:1 compile
		    }
		  }
		},
		watch: {											// Watch for file changes and run appropriate tasks
			styl: {
				files: ['<%= paths.src %>/styl/*.styl'],
				tasks: ['stylus','concat:css','csso']
			},
			scss: {
				files: ['<%= paths.src %>/scss/*.scss'],
				tasks: ['sass','concat:css','csso']
			},
			scripts: {
				files: ['<%= jshint.files %>'],
				tasks: ['jshint','concat:dist','uglify']
			},
		},
		copy: {												// Copy any files into more logical positions for later use
			main: {
				files: [
				  // includes files within path
				  {	
				  	expand: true, 
				  	flatten: true, 
				  	src: ['bower_components/jquery/dist/*'], 
				  	dest: '<%= paths.dist %>/js/jquery/', 
				  	filter: 'isFile'
				  }, {
				  	expand: true, 
				  	flatten: true, 
				  	src: ['<%= paths.src %>/js/lib/*'], 
				  	dest: '<%= paths.dist %>/js/jquery/', 
				  	filter: 'isFile'
				  }, {
				  	expand: true, 
				  	flatten: true, 
				  	src: ['bower_components/nouislider/distribute/jquery.nouislider.all.min.js'], 
				  	dest: '<%= paths.dist %>/js/jquery/', 
				  	filter: 'isFile'
				  }, {
				  	expand: true, 
				  	flatten: true, 
				  	src: ['bower_components/nouislider/distribute/jquery.nouislider.min.css'], 
				  	dest: '<%= paths.dist %>/css/', 
				  	filter: 'isFile'
				  }
				]
			}
		}
	});

	// Laod all NPM packages for tasks
 	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-stylus');
	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-csso');

  	// Default task
 	grunt.registerTask('default', ['sass:dist','stylus','jshint','concat','uglify','csso','copy','watch']);
};