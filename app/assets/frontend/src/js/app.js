/* 
* @Author: Bede
* @Date:   2014-10-02 21:33:23
* @Last Modified by:   Bede
* @Last Modified time: 2014-10-04 21:22:28
*/

// Setup a namespace
var CableGate = CableGate || {
	Controllers : {
		Embassy : {},
		Cloud : {},
		Page : {}
	},
	Models : {
		Map : {},
		Embassy : {},
		Cloud : {},
		Timeline : {}
	}
};

// Setup alias'
var controllers = CableGate.Controllers;
var models = CableGate.Models;

var Embassy = models.Embassy; 
var Cloud = models.Cloud;
var Timeline = models.Timeline;

var EmbassyController = controllers.Embassy;
var CloudController = controllers.Cloud;
var PageController = controllers.Page;

//////////////////
// CLOUD MODEL  //
//////////////////

/**
 * Cloud object represents tag cloud
 * @param {mixed} cloudOptions  id, height and width
 * @param {mixed} canvasOptions see tagcanvas options
 * @return {Cloud} Instance of Cablegate.Models.Cloud
 */
Cloud = function(cloudOptions, canvasOptions) {

	// Set object variables
	this.id = cloudOptions.id;
	this.height = cloudOptions.height;
	this.width = cloudOptions.width;

	// Setup the DOM element attached to cloud
	$('#'+this.id).attr('height',this.height);
	$('#'+this.id).attr('width',this.width);

	// Setup tag canvas using tagcanvas plugin
	$('#'+this.id).tagcanvas(canvasOptions);
};

///////////////////////
// CLOUD CONTROLLER  //
///////////////////////

/**
 * Controller for a Cloud
 * @param {object} years    Years over which to scan for clouds
 * @param {Cloud} model     Cloud model to be controlled
 * @param {Timeline} timeline Timeline model to use
 */
CloudController = function(years, model, timeline) {

	// Setup object variables
	this.years = years;
	this.cloud = model;
	this.timeline = timeline;
};

/**
 * Get period of time to calculate cloud over
 * @return {object} Collection of all years with most popular words for that year
 */
CloudController.prototype.getPeriod = function() {
	
	// Initialise output
	var output = {};

	// Iterate over all years
	for(var year in this.years) {
		// Only output if its within the current timeline limits
		if(year >= this.timeline.limits.min && year <= this.timeline.limits.max) {
			output[year] = this.years[year];
		}
	}
	return output;
};

/**
 * Gets the popular tags of the current cloud
 * @return {object} Contains 15 of the most popular tags over the timeline period
 */
CloudController.prototype.getTags = function() {

	// Basic compare function to be used when sorting
	var compare = function(a,b) {
		if (a.count < b.count)
			return 1;
		if (a.count > b.count)
			return -1;
		return 0;
	};

	// Converts object to array (so can be sorted)
	var toArray = function(obj) {
		var out = [];
		for(var key in obj) {
			// Scale based on the other tags count
			var scaled = Math.round((obj[key] / max) * 10);
			// Store name of tag, its count and its scaled count
			out.push({
				"name" : key,
				"count" : obj[key],
				"scaled" : isFinite(scaled) ? scaled : 5
			});
		}
		return out;
	};

	var period = this.getPeriod();

	// Setup the two empty object to start merging
	var a, b;
	var first = true;
	var max = 0;
	// Iterate over years in the current period of time
	for(var year in period) {
		// If its first, then a will need to be initialised
		if(first) {
			a = period[year];
			first = false;
			continue;
		}
		b = period[year];

		// Merge the two objects, at conflicts add them together
		for(var tag in b) {
			if(a.hasOwnProperty(tag)) {
				a[tag] = a[tag] + b[tag]; //a[tag] > b[tag] ? a[tag] : b[tag];
			} else {
				a[tag] = b[tag];
			}
			// Update the maximum count value
			max = a[tag] > max ? a[tag] : max;
		}
	}

	// Convert the merged object to an array, 
	// 		sort in order of greatest count to smallest
	// 		then return the first 15 items in the array
	var tags = toArray(a).sort(compare).splice(0,15);


	return tags;
};

/**
 * Render the cloud as a html list
 * @return String HTML string describing the list
 */
CloudController.prototype.render = function() {
	var html = '<ul>';
	var tags = this.getTags();
	for(var i = 0; i < tags.length; i++) {
		html += '<li><a data-count="'+tags[i].scaled+'" href="/page/1?search='+tags[i].name+'">'+tags[i].name+'</a></li>';
	}
	html = html + '</ul>';
	return html;
};

/**
 * Refreshes the DOM element for the Cloud model
 * @return void
 */
CloudController.prototype.update = function() {
	$('#'+this.cloud.id).html(this.render());
	$('#'+this.cloud.id).tagcanvas('reload');
};

////////////////////
// TIMELINE MODEL //
////////////////////

/**
 * Timeline model to store years and limits to allow for iteration of given periods of time
 * @param {object} raw    raw time information
 * @param {min, max} limits the upper and lower limits to impose on the years
 */
Timeline = function(raw, limits) { 
	this.max_count = 0;
	this.years = raw; 
	this.limits = limits || { 'min' : Timeline.MIN_YEAR, 'max' : Timeline.MAX_YEAR };
	
	// Initialise an empty callbacks object to store callback functions
	this.callbacks = {};
};

// Define 'static' maximum limitations
Timeline.MIN_YEAR = 1966;
Timeline.MAX_YEAR = 2010;

/**
 * Says the timeline should run its callbacks
 * @return void
 */
Timeline.prototype.updated = function() {
	// Iterate over all set callbacks and execute
	for(var key in this.callbacks) {
		this.callbacks[key]();
	}
};

/**
 * Counts up the values over the years of the current period (limits)
 * @return int Final value of summed years
 */
Timeline.prototype.getCount = function() {
	var count = 0;
	for (var year in this.years) {
  		if (this.years.hasOwnProperty(year)) {
	    	if(year <= this.limits.max && year >= this.limits.min) {
	    		count += parseInt(this.years[year]);
	    	}
  		}
	}

	return count;
};

///////////////////
// EMBASSY MODEL //
///////////////////

/**
 * Model a given Embassy, its name and location and store timeline
 * @param {[type]} _name [description]
 * @param {[type]} raw   [description]
 */
Embassy = function(_name, raw) {
	
	this.name = _name;
	
	this.lat = raw.lat;
	this.lng = raw.lng;
	
	var limits = null;
	// Give it the limits of the current page
	if(typeof page !== 'undefined' && typeof page.timeline !== 'undefined')
		limits = page.timeline.limits;

	this.years = new Timeline(raw.years, limits);
	
	// Count represents number of cables sent from the embassy over the given time period
	this.count = this.years.getCount();

	// Set pages maximum count if necessary
	if(this.count > page.timeline.max_count)
		page.timeline.max_count = this.count;
};

/////////////////////////
// EMBASSY CONTROLLER  //
/////////////////////////

/**
 * 'static' function to process raw embassy data into useable data
 * @param  {JSON} raw Raw data fetched from the backend
 * @return {Object}     Location data of embassies with their count
 */
EmbassyController.processData = function(raw) {
	var data = [];
	var count = 0;
	var missing = 0;
	
	// Iterate over each piece of data
	for(var name in raw) {
		// Process it as an embassy
		var emb = new Embassy(name, raw[name]);

		// If the embassy had any cables, and it has a valid location, add it to output
		if((emb.count > 0) && !(parseInt(emb.lat) === 0 && parseInt(emb.lng) === 0))
			data[count++] = emb;
		else
			missing++;
	}

	// Report how many could not be processed
	if(missing > 0)
		console.log(missing+' embassies\' were ommitted.');

	return data;
};

/////////
// MAP //
/////////

/**
 * Model the DOM Map
 * Couldn't use 'Map' as it's reserved
 * @param string 	divID    	DOM Element ID
 * @param JSON 		raw      	Raw JSON Data
 * @param Timeline 	timeline 	Timeline object to use
 */
models.Map = function(divID, raw, timeline) {

	this.timeline = timeline;

	this.data = raw;

	// Options for google map
	this.mapOptions = {
		center: { lat: 0, lng: 0},
		zoom: 2,
		streetViewControl: false,
		mapTypeId: google.maps.MapTypeId.ROADMAP,
    	styles: models.Map.style
	};

	// Set internal map
	this.map = new google.maps.Map(document.getElementById(divID), this.mapOptions);

	// Setup heatmap overlay (heatmap.js)
	this.heatmap = new HeatmapOverlay(this.map, {
		    // radius should be small ONLY if scaleRadius is true (or small radius is intended)
		    "radius": 7,
		    "maxOpacity": 1, 

		    // scales the radius based on map zoom
		    "scaleRadius": true, 
		    // if set to false the heatmap uses the global maximum for colorization
		    // if activated: uses the data maximum within the current map boundaries 
		    //   (there will always be a red spot with useLocalExtremas true)
		    "useLocalExtrema": false,
		    // which field name in your data represents the latitude - default "lat"
		    latField: 'lat',
		    // which field name in your data represents the longitude - default "lng"
		    lngField: 'lng',
		    // which field name in your data represents the data value - default "value"
		    valueField: 'count',
		    blur: 0.99,
		    gradient: {
			    '.2': '#f39c12',
			    '.5': '#d35400',
			    '.8': '#c0392b',
			    '1'	: '#A00F00'
			}
		}
	);

	// Scale the heatmap down
	this.heatmap.scale = 0.1;

	// Init / refresh the map
	this.refresh();
};

/**
 * Refreshes the current map
 * @return void
 */
models.Map.prototype.refresh = function() {
	// Reset the count, incase the next max is different
	this.timeline.max_count = 0;
	
	// Process the raw data into embassies
	var processedData = EmbassyController.processData(this.data);
	
	// Set the heatmap data to processed data, and set limits
	this.heatmap.setData({
		max : this.timeline.max_count*this.heatmap.scale,
		min : 1,
		data : processedData
	});
};

// Style the map, 
// Sourced from http://snazzymaps.com/style/15/subtle-grayscale
models.Map.style = [{
	"featureType":"water",
	"elementType":"all",
	"stylers":[
		{ "hue":"#bbbbbb" },
		{ "saturation": -100 },
		{ "lightness": -4 },
		{ "visibility":"on" }
	]
}, { 
	"featureType":"landscape",
	"elementType":"all",
	"stylers":[
		{"hue":"#999999"},
		{"saturation":-100},
		{"lightness":-33},
		{"visibility":"on"}
	]
}, {
	"featureType":"road",
	"elementType":"all",
	"stylers":[
		{"hue":"#999999"},
		{"saturation":-100},
		{"lightness":-6},
		{"visibility":"on"}
	]
}, {
	"featureType":"poi",
	"elementType":"all",
	"stylers":[
		{"hue":"#aaaaaa"},
		{"saturation":-100},
		{"lightness":-15},
		{"visibility":"on"}
	]
}];

//////////////////////
// PAGE CONTROLLER  //
//////////////////////

/**
 * Page Controller Object
 * Used to generally manage the pages
 */
PageController = function() {

	// Initialise current page
	this.page = '';
	var pages = ['single','browse','map','cloud'];
	var setPage = function(page, index, array) {
		if($('.container').hasClass(page))
			this.page = page;
	};
	pages.forEach(setPage, this);

	// Initialisation functions for each page
	
	var initBrowse = function() {
		// Setup browse links
		$('tbody > tr').click(function() {
			window.location.href = $(this).data('link');
		});
	};

	var initCloud = function(that) {

		that.cloud = new Cloud({
			id: 'cloudCanvas',
			height : $('.container').height() - parseInt($('.container').css('padding-top')),
			width : $(window).width()
		},{
			textColour : '#8BC5B3',
			textFont : 'Inconsolata',
			outlineColour: '#8BC5B3',
			outlineRadius : '2',
			maxSpeed : 0.03,
			depth : 0.75,
			zoom : 0.8,
			weight: true,
			weightFrom: 'data-count',
			weightSize: 10,		
		});
		
		if(typeof that.timeline !== 'undefined' || that.timeline !== null) {

			var process = function(data) {
				var cloudControl = new CloudController(data, that.cloud, that.timeline);
				cloudControl.update();

				that.timeline.callbacks.cloud = function() {
					cloudControl.update();
				};

				$('#timeline').removeClass('disabled');
			};

			$.get('/json/cloud', process);

		}
	};

	var initMap = function(that) {
		
		if(typeof that.timeline == 'undefined' || that.timeline === null)
			throw "No timeline defined";

		var process = function(data) {
			that.timeline.years = data.years;
			that.map = new models.Map('map-canvas', data, that.timeline);
			$('#timeline').removeClass('disabled');
		};

		$.get('/json/map',process);

		that.timeline.callbacks.map = function() {
			that.map.refresh();
		};
	};

	var initTimeline = function(that) {
		that.timeline = new Timeline();

		$('#timeline-control').noUiSlider({
			start: [Timeline.MIN_YEAR, Timeline.MAX_YEAR],
			step: 1,
			range: {
				'min': Timeline.MIN_YEAR,
				'max': Timeline.MAX_YEAR
			}
		});

		// Setup slider
		$('#timeline-control').on('slide',function() {
			that.timeline.limits.max = $(this).val()[1];
			that.timeline.limits.min = $(this).val()[0];
			if($('#max-year').length > 0)
				$('#max-year').text(Math.round($(this).val()[1]));
			if($('#min-year').length > 0)
				$('#min-year').text(Math.round($(this).val()[0]));
			that.timeline.updated();
		});
	};

	// Initialise the timeline if necessary
	if($('#timeline-control').length > 0) { initTimeline(this); }

	// Run the right initialiser
	switch(this.page) {
		case 'browse':
			initBrowse();
			break;
		case 'map':
			initMap(this);
			break;
		case 'cloud':
			initCloud(this);
			break;
		default:
	}
};

///////////////////
// Start 'er up! //
///////////////////

// Make the page variable globally accessible
var page;

// Start up the webpage when the document is ready
$(document).ready(function() {
	page = new PageController();
});
