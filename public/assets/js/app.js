if (!Array.prototype.filter) {
  Array.prototype.filter = function(fun/*, thisArg*/) {
    'use strict';

    if (this === void 0 || this === null) {
      throw new TypeError();
    }

    var t = Object(this);
    var len = t.length >>> 0;
    if (typeof fun !== 'function') {
      throw new TypeError();
    }

    var res = [];
    var thisArg = arguments.length >= 2 ? arguments[1] : void 0;
    for (var i = 0; i < len; i++) {
      if (i in t) {
        var val = t[i];

        // NOTE: Technically this should Object.defineProperty at
        //       the next index, as push can be affected by
        //       properties on Object.prototype and Array.prototype.
        //       But that method's new, and collisions should be
        //       rare, so use the more-compatible alternative.
        if (fun.call(thisArg, val, i, t)) {
          res.push(val);
        }
      }
    }

    return res;
  };
};// Production steps of ECMA-262, Edition 5, 15.4.4.21
// Reference: http://es5.github.io/#x15.4.4.21
if (!Array.prototype.reduce) {
  Array.prototype.reduce = function(callback /*, initialValue*/) {
    'use strict';
    if (this == null) {
      throw new TypeError('Array.prototype.reduce called on null or undefined');
    }
    if (typeof callback !== 'function') {
      throw new TypeError(callback + ' is not a function');
    }
    var t = Object(this), len = t.length >>> 0, k = 0, value;
    if (arguments.length == 2) {
      value = arguments[1];
    } else {
      while (k < len && ! k in t) {
        k++; 
      }
      if (k >= len) {
        throw new TypeError('Reduce of empty array with no initial value');
      }
      value = t[k++];
    }
    for (; k < len; k++) {
      if (k in t) {
        value = callback(value, t[k], k, t);
      }
    }
    return value;
  };
};/* 
* @Author: Bede
* @Date:   2014-10-02 21:33:23
* @Last Modified by:   Bede
* @Last Modified time: 2014-10-04 21:22:28
*/

// Setup a namespace
var CableGate = CableGate || {
	Controllers : {
		Embassy : {},
		Cloud : {},
		Page : {}
	},
	Models : {
		Map : {},
		Embassy : {},
		Cloud : {},
		Timeline : {}
	}
};

// Setup alias'
var controllers = CableGate.Controllers;
var models = CableGate.Models;

var Embassy = models.Embassy; 
var Cloud = models.Cloud;
var Timeline = models.Timeline;

var EmbassyController = controllers.Embassy;
var CloudController = controllers.Cloud;
var PageController = controllers.Page;

//////////////////
// CLOUD MODEL  //
//////////////////

/**
 * Cloud object represents tag cloud
 * @param {mixed} cloudOptions  id, height and width
 * @param {mixed} canvasOptions see tagcanvas options
 * @return {Cloud} Instance of Cablegate.Models.Cloud
 */
Cloud = function(cloudOptions, canvasOptions) {

	// Set object variables
	this.id = cloudOptions.id;
	this.height = cloudOptions.height;
	this.width = cloudOptions.width;

	// Setup the DOM element attached to cloud
	$('#'+this.id).attr('height',this.height);
	$('#'+this.id).attr('width',this.width);

	// Setup tag canvas using tagcanvas plugin
	$('#'+this.id).tagcanvas(canvasOptions);
};

///////////////////////
// CLOUD CONTROLLER  //
///////////////////////

/**
 * Controller for a Cloud
 * @param {object} years    Years over which to scan for clouds
 * @param {Cloud} model     Cloud model to be controlled
 * @param {Timeline} timeline Timeline model to use
 */
CloudController = function(years, model, timeline) {

	// Setup object variables
	this.years = years;
	this.cloud = model;
	this.timeline = timeline;
};

/**
 * Get period of time to calculate cloud over
 * @return {object} Collection of all years with most popular words for that year
 */
CloudController.prototype.getPeriod = function() {
	
	// Initialise output
	var output = {};

	// Iterate over all years
	for(var year in this.years) {
		// Only output if its within the current timeline limits
		if(year >= this.timeline.limits.min && year <= this.timeline.limits.max) {
			output[year] = this.years[year];
		}
	}
	return output;
};

/**
 * Gets the popular tags of the current cloud
 * @return {object} Contains 15 of the most popular tags over the timeline period
 */
CloudController.prototype.getTags = function() {

	// Basic compare function to be used when sorting
	var compare = function(a,b) {
		if (a.count < b.count)
			return 1;
		if (a.count > b.count)
			return -1;
		return 0;
	};

	// Converts object to array (so can be sorted)
	var toArray = function(obj) {
		var out = [];
		for(var key in obj) {
			// Scale based on the other tags count
			var scaled = Math.round((obj[key] / max) * 10);
			// Store name of tag, its count and its scaled count
			out.push({
				"name" : key,
				"count" : obj[key],
				"scaled" : isFinite(scaled) ? scaled : 5
			});
		}
		return out;
	};

	var period = this.getPeriod();

	// Setup the two empty object to start merging
	var a, b;
	var first = true;
	var max = 0;
	// Iterate over years in the current period of time
	for(var year in period) {
		// If its first, then a will need to be initialised
		if(first) {
			a = period[year];
			first = false;
			continue;
		}
		b = period[year];

		// Merge the two objects, at conflicts add them together
		for(var tag in b) {
			if(a.hasOwnProperty(tag)) {
				a[tag] = a[tag] + b[tag]; //a[tag] > b[tag] ? a[tag] : b[tag];
			} else {
				a[tag] = b[tag];
			}
			// Update the maximum count value
			max = a[tag] > max ? a[tag] : max;
		}
	}

	// Convert the merged object to an array, 
	// 		sort in order of greatest count to smallest
	// 		then return the first 15 items in the array
	var tags = toArray(a).sort(compare).splice(0,15);


	return tags;
};

/**
 * Render the cloud as a html list
 * @return String HTML string describing the list
 */
CloudController.prototype.render = function() {
	var html = '<ul>';
	var tags = this.getTags();
	for(var i = 0; i < tags.length; i++) {
		html += '<li><a data-count="'+tags[i].scaled+'" href="/page/1?search='+tags[i].name+'">'+tags[i].name+'</a></li>';
	}
	html = html + '</ul>';
	return html;
};

/**
 * Refreshes the DOM element for the Cloud model
 * @return void
 */
CloudController.prototype.update = function() {
	$('#'+this.cloud.id).html(this.render());
	$('#'+this.cloud.id).tagcanvas('reload');
};

////////////////////
// TIMELINE MODEL //
////////////////////

/**
 * Timeline model to store years and limits to allow for iteration of given periods of time
 * @param {object} raw    raw time information
 * @param {min, max} limits the upper and lower limits to impose on the years
 */
Timeline = function(raw, limits) { 
	this.max_count = 0;
	this.years = raw; 
	this.limits = limits || { 'min' : Timeline.MIN_YEAR, 'max' : Timeline.MAX_YEAR };
	
	// Initialise an empty callbacks object to store callback functions
	this.callbacks = {};
};

// Define 'static' maximum limitations
Timeline.MIN_YEAR = 1966;
Timeline.MAX_YEAR = 2010;

/**
 * Says the timeline should run its callbacks
 * @return void
 */
Timeline.prototype.updated = function() {
	// Iterate over all set callbacks and execute
	for(var key in this.callbacks) {
		this.callbacks[key]();
	}
};

/**
 * Counts up the values over the years of the current period (limits)
 * @return int Final value of summed years
 */
Timeline.prototype.getCount = function() {
	var count = 0;
	for (var year in this.years) {
  		if (this.years.hasOwnProperty(year)) {
	    	if(year <= this.limits.max && year >= this.limits.min) {
	    		count += parseInt(this.years[year]);
	    	}
  		}
	}

	return count;
};

///////////////////
// EMBASSY MODEL //
///////////////////

/**
 * Model a given Embassy, its name and location and store timeline
 * @param {[type]} _name [description]
 * @param {[type]} raw   [description]
 */
Embassy = function(_name, raw) {
	
	this.name = _name;
	
	this.lat = raw.lat;
	this.lng = raw.lng;
	
	var limits = null;
	// Give it the limits of the current page
	if(typeof page !== 'undefined' && typeof page.timeline !== 'undefined')
		limits = page.timeline.limits;

	this.years = new Timeline(raw.years, limits);
	
	// Count represents number of cables sent from the embassy over the given time period
	this.count = this.years.getCount();

	// Set pages maximum count if necessary
	if(this.count > page.timeline.max_count)
		page.timeline.max_count = this.count;
};

/////////////////////////
// EMBASSY CONTROLLER  //
/////////////////////////

/**
 * 'static' function to process raw embassy data into useable data
 * @param  {JSON} raw Raw data fetched from the backend
 * @return {Object}     Location data of embassies with their count
 */
EmbassyController.processData = function(raw) {
	var data = [];
	var count = 0;
	var missing = 0;
	
	// Iterate over each piece of data
	for(var name in raw) {
		// Process it as an embassy
		var emb = new Embassy(name, raw[name]);

		// If the embassy had any cables, and it has a valid location, add it to output
		if((emb.count > 0) && !(parseInt(emb.lat) === 0 && parseInt(emb.lng) === 0))
			data[count++] = emb;
		else
			missing++;
	}

	// Report how many could not be processed
	if(missing > 0)
		console.log(missing+' embassies\' were ommitted.');

	return data;
};

/////////
// MAP //
/////////

/**
 * Model the DOM Map
 * Couldn't use 'Map' as it's reserved
 * @param string 	divID    	DOM Element ID
 * @param JSON 		raw      	Raw JSON Data
 * @param Timeline 	timeline 	Timeline object to use
 */
models.Map = function(divID, raw, timeline) {

	this.timeline = timeline;

	this.data = raw;

	// Options for google map
	this.mapOptions = {
		center: { lat: 0, lng: 0},
		zoom: 2,
		streetViewControl: false,
		mapTypeId: google.maps.MapTypeId.ROADMAP,
    	styles: models.Map.style
	};

	// Set internal map
	this.map = new google.maps.Map(document.getElementById(divID), this.mapOptions);

	// Setup heatmap overlay (heatmap.js)
	this.heatmap = new HeatmapOverlay(this.map, {
		    // radius should be small ONLY if scaleRadius is true (or small radius is intended)
		    "radius": 7,
		    "maxOpacity": 1, 

		    // scales the radius based on map zoom
		    "scaleRadius": true, 
		    // if set to false the heatmap uses the global maximum for colorization
		    // if activated: uses the data maximum within the current map boundaries 
		    //   (there will always be a red spot with useLocalExtremas true)
		    "useLocalExtrema": false,
		    // which field name in your data represents the latitude - default "lat"
		    latField: 'lat',
		    // which field name in your data represents the longitude - default "lng"
		    lngField: 'lng',
		    // which field name in your data represents the data value - default "value"
		    valueField: 'count',
		    blur: 0.99,
		    gradient: {
			    '.2': '#f39c12',
			    '.5': '#d35400',
			    '.8': '#c0392b',
			    '1'	: '#A00F00'
			}
		}
	);

	// Scale the heatmap down
	this.heatmap.scale = 0.1;

	// Init / refresh the map
	this.refresh();
};

/**
 * Refreshes the current map
 * @return void
 */
models.Map.prototype.refresh = function() {
	// Reset the count, incase the next max is different
	this.timeline.max_count = 0;
	
	// Process the raw data into embassies
	var processedData = EmbassyController.processData(this.data);
	
	// Set the heatmap data to processed data, and set limits
	this.heatmap.setData({
		max : this.timeline.max_count*this.heatmap.scale,
		min : 1,
		data : processedData
	});
};

// Style the map, 
// Sourced from http://snazzymaps.com/style/15/subtle-grayscale
models.Map.style = [{
	"featureType":"water",
	"elementType":"all",
	"stylers":[
		{ "hue":"#bbbbbb" },
		{ "saturation": -100 },
		{ "lightness": -4 },
		{ "visibility":"on" }
	]
}, { 
	"featureType":"landscape",
	"elementType":"all",
	"stylers":[
		{"hue":"#999999"},
		{"saturation":-100},
		{"lightness":-33},
		{"visibility":"on"}
	]
}, {
	"featureType":"road",
	"elementType":"all",
	"stylers":[
		{"hue":"#999999"},
		{"saturation":-100},
		{"lightness":-6},
		{"visibility":"on"}
	]
}, {
	"featureType":"poi",
	"elementType":"all",
	"stylers":[
		{"hue":"#aaaaaa"},
		{"saturation":-100},
		{"lightness":-15},
		{"visibility":"on"}
	]
}];

//////////////////////
// PAGE CONTROLLER  //
//////////////////////

/**
 * Page Controller Object
 * Used to generally manage the pages
 */
PageController = function() {

	// Initialise current page
	this.page = '';
	var pages = ['single','browse','map','cloud'];
	var setPage = function(page, index, array) {
		if($('.container').hasClass(page))
			this.page = page;
	};
	pages.forEach(setPage, this);

	// Initialisation functions for each page
	
	var initBrowse = function() {
		// Setup browse links
		$('tbody > tr').click(function() {
			window.location.href = $(this).data('link');
		});
	};

	var initCloud = function(that) {

		that.cloud = new Cloud({
			id: 'cloudCanvas',
			height : $('.container').height() - parseInt($('.container').css('padding-top')),
			width : $(window).width()
		},{
			textColour : '#8BC5B3',
			textFont : 'Inconsolata',
			outlineColour: '#8BC5B3',
			outlineRadius : '2',
			maxSpeed : 0.03,
			depth : 0.75,
			zoom : 0.8,
			weight: true,
			weightFrom: 'data-count',
			weightSize: 10,		
		});
		
		if(typeof that.timeline !== 'undefined' || that.timeline !== null) {

			var process = function(data) {
				var cloudControl = new CloudController(data, that.cloud, that.timeline);
				cloudControl.update();

				that.timeline.callbacks.cloud = function() {
					cloudControl.update();
				};

				$('#timeline').removeClass('disabled');
			};

			$.get('/json/cloud', process);

		}
	};

	var initMap = function(that) {
		
		if(typeof that.timeline == 'undefined' || that.timeline === null)
			throw "No timeline defined";

		var process = function(data) {
			that.timeline.years = data.years;
			that.map = new models.Map('map-canvas', data, that.timeline);
			$('#timeline').removeClass('disabled');
		};

		$.get('/json/map',process);

		that.timeline.callbacks.map = function() {
			that.map.refresh();
		};
	};

	var initTimeline = function(that) {
		that.timeline = new Timeline();

		$('#timeline-control').noUiSlider({
			start: [Timeline.MIN_YEAR, Timeline.MAX_YEAR],
			step: 1,
			range: {
				'min': Timeline.MIN_YEAR,
				'max': Timeline.MAX_YEAR
			}
		});

		// Setup slider
		$('#timeline-control').on('slide',function() {
			that.timeline.limits.max = $(this).val()[1];
			that.timeline.limits.min = $(this).val()[0];
			if($('#max-year').length > 0)
				$('#max-year').text(Math.round($(this).val()[1]));
			if($('#min-year').length > 0)
				$('#min-year').text(Math.round($(this).val()[0]));
			that.timeline.updated();
		});
	};

	// Initialise the timeline if necessary
	if($('#timeline-control').length > 0) { initTimeline(this); }

	// Run the right initialiser
	switch(this.page) {
		case 'browse':
			initBrowse();
			break;
		case 'map':
			initMap(this);
			break;
		case 'cloud':
			initCloud(this);
			break;
		default:
	}
};

///////////////////
// Start 'er up! //
///////////////////

// Make the page variable globally accessible
var page;

// Start up the webpage when the document is ready
$(document).ready(function() {
	page = new PageController();
});
;/*
* heatmap.js gmaps overlay
*
* Copyright (c) 2014, Patrick Wied (http://www.patrick-wied.at)
* Dual-licensed under the MIT (http://www.opensource.org/licenses/mit-license.php)
* and the Beerware (http://en.wikipedia.org/wiki/Beerware) license.
*/

function HeatmapOverlay(map, cfg){ 
  this.setMap(map);
  this.initialize(cfg || {});
}

HeatmapOverlay.prototype = new google.maps.OverlayView();


HeatmapOverlay.CSS_TRANSFORM = (function() {
  var div = document.createElement('div');
  var props = [
    'transform',
    'WebkitTransform',
    'MozTransform',
    'OTransform',
    'msTransform'
  ];

  for (var i = 0; i < props.length; i++) {
    var prop = props[i];
    if (div.style[prop] !== undefined) {
      return prop;
    }
  }

  return props[0];
})();

HeatmapOverlay.prototype.initialize = function(cfg) {
  this.cfg = cfg;
  
  var map = this.map = this.getMap();
  var container = this.container = document.createElement('div');
  var mapDiv = map.getDiv();
  var width = this.width = mapDiv.clientWidth;
  var height = this.height = mapDiv.clientHeight;

  container.style.cssText = 'width:' + width +'px;height:' + height+'px;';

  this.data = [];
  this.max = 1;
  this.min = 0;

  cfg.container = container;
};

HeatmapOverlay.prototype.onAdd = function(){

  this.getPanes().overlayLayer.appendChild(this.container);

  this.changeHandler = google.maps.event.addListener(
    this.map,
    'bounds_changed',
    this.draw
  );
 
  if (!this.heatmap) {
    this.heatmap = h337.create(this.cfg);
  }
  this.draw();
};

HeatmapOverlay.prototype.onRemove = function() { 
  if (!this.map) { return; }

  this.map = null;

  this.container.parentElement.removeChild(this.container);

  if (this.changeHandler) {
    google.maps.event.removeListener(this.changeHandler);
    this.changeHandler = null;
  }

};

HeatmapOverlay.prototype.draw = function() {
  if (!this.map) { return; }

  var bounds = this.map.getBounds();

  var topLeft = new google.maps.LatLng(
    bounds.getNorthEast().lat(),
    bounds.getSouthWest().lng()
  );

  var projection = this.getProjection();
  var point = projection.fromLatLngToDivPixel(topLeft);

  this.container.style[HeatmapOverlay.CSS_TRANSFORM] = 'translate(' +
      Math.round(point.x) + 'px,' +
      Math.round(point.y) + 'px)';

  this.update();
};

HeatmapOverlay.prototype.resize = function() {

  if (!this.map){ return; }

  var div = this.map.getDiv(),
    width = div.clientWidth,
    height = div.clientHeight;

  if (width == this.width && height == this.height){ return; }

  this.width = width;
  this.height = height;

  // update heatmap dimensions
  this.heatmap._renderer.setDimensions(width, height);
  // then redraw all datapoints with update
  this.update();
};

HeatmapOverlay.prototype.update = function() {
  var projection = this.map.getProjection(),
    zoom, scale, bounds, topLeft;

  if (!projection){ return; }

  bounds = this.map.getBounds();

  topLeft = new google.maps.LatLng(
    bounds.getNorthEast().lat(),
    bounds.getSouthWest().lng()
  );

  zoom = this.map.getZoom();
  scale = Math.pow(2, zoom);

  this.resize();

  if (this.data.length === 0) {
    return;
  }

  var generatedData = { max: this.max, min: this.min };
  var latLngPoints = [];
  // iterate through data 
  var len = this.data.length;
  var layerProjection = this.getProjection();
  var layerOffset = layerProjection.fromLatLngToDivPixel(topLeft);
  var radiusMultiplier = this.cfg.scaleRadius ? scale : 1;
  var localMax = 0;
  var localMin = 0;
  var valueField = this.cfg.valueField;


  while (len--) {
    var entry = this.data[len];
    var value = entry[valueField];
    var latlng = entry.latlng;


    // we don't wanna render points that are not even on the map ;-)
    if (!bounds.contains(latlng)) {
      continue;
    }
    // local max is the maximum within current bounds
    localMax = Math.max(value, localMax);
    localMin = Math.min(value, localMin);

    var point = this.pixelTransform(layerProjection.fromLatLngToDivPixel(latlng));
    var latlngPoint = { x: Math.round(point.x - layerOffset.x), y: Math.round(point.y - layerOffset.y) };
    latlngPoint[valueField] = value;

    var radius;

    if (entry.radius) {
      radius = entry.radius * radiusMultiplier;
    } else {
      radius = (this.cfg.radius || 2) * radiusMultiplier;
    }
    latlngPoint.radius = radius;
    latLngPoints.push(latlngPoint);
  }
  if (this.cfg.useLocalExtrema) {
    generatedData.max = localMax;
    generatedData.min = localMin;
  }

  generatedData.data = latLngPoints;

  this.heatmap.setData(generatedData);

};

HeatmapOverlay.prototype.pixelTransform = function(point) {
  if (point.x < 0) {
    point.x += this.width;
  }
  if (point.x > this.width) {
    point.x -= this.width;
  }
  if (point.y < 0) {
    point.y += this.height;
  }
  if (point.y > this.height) {
    point.y -= this.height;
  }
  return point;
};

HeatmapOverlay.prototype.setData = function(data) { 
  this.max = data.max;
  this.min = data.min;

  var latField = this.cfg.latField || 'lat';
  var lngField = this.cfg.lngField || 'lng';
  var valueField = this.cfg.valueField || 'value';

  // transform data to latlngs
  var data = data.data;
  var len = data.length;
  var d = [];

  while (len--) {
    var entry = data[len];
    var latlng = new google.maps.LatLng(entry[latField], entry[lngField]);
    var dataObj = { latlng: latlng };
    dataObj[valueField] = entry[valueField];
    if (entry.radius) {
      dataObj.radius = entry.radius;
    }
    d.push(dataObj);
  }
  this.data = d;
  this.update();
};
// experimential. not ready yet.
HeatmapOverlay.prototype.addData = function(pointOrArray) {
  if (pointOrArray.length > 0) {
      var len = pointOrArray.length;
      while(len--) {
        this.addData(pointOrArray[len]);
      }
    } else {
      var latField = this.cfg.latField || 'lat';
      var lngField = this.cfg.lngField || 'lng';
      var valueField = this.cfg.valueField || 'value';
      var entry = pointOrArray;
      var latlng = new google.maps.LatLng(entry[latField], entry[lngField]);
      var dataObj = { latlng: latlng };
      
      dataObj[valueField] = entry[valueField];
      if (entry.radius) {
        dataObj.radius = entry.radius;
      }
      this.max = Math.max(this.max, dataObj[valueField]);
      this.min = Math.min(this.min, dataObj[valueField]);
      this.data.push(dataObj);
      this.update();
    }
};
;/*
 * heatmap.js v2.0.0 | JavaScript Heatmap Library
 *
 * Copyright 2008-2014 Patrick Wied <heatmapjs@patrick-wied.at> - All rights reserved.
 * Dual licensed under MIT and Beerware license 
 *
 * :: 2014-09-04 17:52
 */
(function(a){var b={defaultRadius:40,defaultRenderer:"canvas2d",defaultGradient:{.25:"rgb(0,0,255)",.55:"rgb(0,255,0)",.85:"yellow",1:"rgb(255,0,0)"},defaultMaxOpacity:1,defaultMinOpacity:0,defaultBlur:.85,defaultXField:"x",defaultYField:"y",defaultValueField:"value",plugins:{}};var c=function i(){var a=function d(a){this._coordinator={};this._data=[];this._radi=[];this._min=0;this._max=1;this._xField=a["xField"]||a.defaultXField;this._yField=a["yField"]||a.defaultYField;this._valueField=a["valueField"]||a.defaultValueField;if(a["radius"]){this._cfgRadius=a["radius"]}};var c=b.defaultRadius;a.prototype={_organiseData:function(a,b){var d=a[this._xField];var e=a[this._yField];var f=this._radi;var g=this._data;var h=this._max;var i=this._min;var j=a[this._valueField]||1;var k=a.radius||this._cfgRadius||c;if(!g[d]){g[d]=[];f[d]=[]}if(!g[d][e]){g[d][e]=j;f[d][e]=k}else{g[d][e]+=j}if(g[d][e]>h){if(!b){this._max=g[d][e]}else{this.setDataMax(g[d][e])}return false}else{return{x:d,y:e,value:j,radius:k,min:i,max:h}}},_unOrganizeData:function(){var a=[];var b=this._data;var c=this._radi;for(var d in b){for(var e in b[d]){a.push({x:d,y:e,radius:c[d][e],value:b[d][e]})}}return{min:this._min,max:this._max,data:a}},_onExtremaChange:function(){this._coordinator.emit("extremachange",{min:this._min,max:this._max})},addData:function(){if(arguments[0].length>0){var a=arguments[0];var b=a.length;while(b--){this.addData.call(this,a[b])}}else{var c=this._organiseData(arguments[0],true);if(c){this._coordinator.emit("renderpartial",{min:this._min,max:this._max,data:[c]})}}return this},setData:function(a){var b=a.data;var c=b.length;this._data=[];this._radi=[];for(var d=0;d<c;d++){this._organiseData(b[d],false)}this._max=a.max;this._min=a.min||0;this._onExtremaChange();this._coordinator.emit("renderall",this._getInternalData());return this},removeData:function(){},setDataMax:function(a){this._max=a;this._onExtremaChange();this._coordinator.emit("renderall",this._getInternalData());return this},setDataMin:function(a){this._min=a;this._onExtremaChange();this._coordinator.emit("renderall",this._getInternalData());return this},setCoordinator:function(a){this._coordinator=a},_getInternalData:function(){return{max:this._max,min:this._min,data:this._data,radi:this._radi}},getData:function(){return this._unOrganizeData()}};return a}();var d=function j(){var a=function(a){var b=a.gradient||a.defaultGradient;var c=document.createElement("canvas");var d=c.getContext("2d");c.width=256;c.height=1;var e=d.createLinearGradient(0,0,256,1);for(var f in b){e.addColorStop(f,b[f])}d.fillStyle=e;d.fillRect(0,0,256,1);return d.getImageData(0,0,256,1).data};var b=function(a,b){var c=document.createElement("canvas");var d=c.getContext("2d");var e=a;var f=a;c.width=c.height=a*2;if(b==1){d.beginPath();d.arc(e,f,a,0,2*Math.PI,false);d.fillStyle="rgba(0,0,0,1)";d.fill()}else{var g=d.createRadialGradient(e,f,a*b,e,f,a);g.addColorStop(0,"rgba(0,0,0,1)");g.addColorStop(1,"rgba(0,0,0,0)");d.fillStyle=g;d.fillRect(0,0,2*a,2*a)}return c};var c=function(a){var b=[];var c=a.min;var d=a.max;var e=a.radi;var a=a.data;var f=Object.keys(a);var g=f.length;while(g--){var h=f[g];var i=Object.keys(a[h]);var j=i.length;while(j--){var k=i[j];var l=a[h][k];var m=e[h][k];b.push({x:h,y:k,value:l,radius:m})}}return{min:c,max:d,data:b}};function d(b){var c=b.container;var d=this.shadowCanvas=document.createElement("canvas");var e=this.canvas=b.canvas||document.createElement("canvas");var f=this._renderBoundaries=[1e4,1e4,0,0];var g=getComputedStyle(b.container)||{};e.className="heatmap-canvas";this._width=e.width=d.width=+g.width.replace(/px/,"");this._height=e.height=d.height=+g.height.replace(/px/,"");this.shadowCtx=d.getContext("2d");this.ctx=e.getContext("2d");e.style.cssText=d.style.cssText="position:absolute;left:0;top:0;";c.style.position="relative";c.appendChild(e);this._palette=a(b);this._templates={};this._setStyles(b)}d.prototype={renderPartial:function(a){this._drawAlpha(a);this._colorize()},renderAll:function(a){this._clear();this._drawAlpha(c(a));this._colorize()},_updateGradient:function(b){this._palette=a(b)},updateConfig:function(a){if(a["gradient"]){this._updateGradient(a)}this._setStyles(a)},setDimensions:function(a,b){this._width=a;this._height=b;this.canvas.width=this.shadowCanvas.width=a;this.canvas.height=this.shadowCanvas.height=b},_clear:function(){this.shadowCtx.clearRect(0,0,this._width,this._height);this.ctx.clearRect(0,0,this._width,this._height)},_setStyles:function(a){this._blur=a.blur==0?0:a.blur||a.defaultBlur;if(a.backgroundColor){this.canvas.style.backgroundColor=a.backgroundColor}this._opacity=(a.opacity||0)*255;this._maxOpacity=(a.maxOpacity||a.defaultMaxOpacity)*255;this._minOpacity=(a.minOpacity||a.defaultMinOpacity)*255;this._useGradientOpacity=!!a.useGradientOpacity},_drawAlpha:function(a){var c=this._min=a.min;var d=this._max=a.max;var a=a.data||[];var e=a.length;var f=1-this._blur;while(e--){var g=a[e];var h=g.x;var i=g.y;var j=g.radius;var k=Math.min(g.value,d);var l=h-j;var m=i-j;var n=this.shadowCtx;var o;if(!this._templates[j]){this._templates[j]=o=b(j,f)}else{o=this._templates[j]}n.globalAlpha=(k-c)/(d-c);n.drawImage(o,l,m);if(l<this._renderBoundaries[0]){this._renderBoundaries[0]=l}if(m<this._renderBoundaries[1]){this._renderBoundaries[1]=m}if(l+2*j>this._renderBoundaries[2]){this._renderBoundaries[2]=l+2*j}if(m+2*j>this._renderBoundaries[3]){this._renderBoundaries[3]=m+2*j}}},_colorize:function(){var a=this._renderBoundaries[0];var b=this._renderBoundaries[1];var c=this._renderBoundaries[2]-a;var d=this._renderBoundaries[3]-b;var e=this._width;var f=this._height;var g=this._opacity;var h=this._maxOpacity;var i=this._minOpacity;var j=this._useGradientOpacity;if(a<0){a=0}if(b<0){b=0}if(a+c>e){c=e-a}if(b+d>f){d=f-b}var k=this.shadowCtx.getImageData(a,b,c,d);var l=k.data;var m=l.length;var n=this._palette;for(var o=3;o<m;o+=4){var p=l[o];var q=p*4;if(!q){continue}var r;if(g>0){r=g}else{if(p<h){if(p<i){r=i}else{r=p}}else{r=h}}l[o-3]=n[q];l[o-2]=n[q+1];l[o-1]=n[q+2];l[o]=j?n[q+3]:r}k.data=l;this.ctx.putImageData(k,a,b);this._renderBoundaries=[1e3,1e3,0,0]},getValueAt:function(a){var b;var c=this.shadowCtx;var d=c.getImageData(a.x,a.y,1,1);var e=d.data[3];var f=this._max;var g=this._min;b=Math.abs(f-g)*(e/255)>>0;return b},getDataURL:function(){return this.canvas.toDataURL()}};return d}();var e=function k(){var a=false;if(b["defaultRenderer"]==="canvas2d"){a=d}return a}();var f={merge:function(){var a={};var b=arguments.length;for(var c=0;c<b;c++){var d=arguments[c];for(var e in d){a[e]=d[e]}}return a}};var g=function l(){var a=function h(){function a(){this.cStore={}}a.prototype={on:function(a,b,c){var d=this.cStore;if(!d[a]){d[a]=[]}d[a].push(function(a){return b.call(c,a)})},emit:function(a,b){var c=this.cStore;if(c[a]){var d=c[a].length;for(var e=0;e<d;e++){var f=c[a][e];f(b)}}}};return a}();var d=function(a){var b=a._renderer;var c=a._coordinator;var d=a._store;c.on("renderpartial",b.renderPartial,b);c.on("renderall",b.renderAll,b);c.on("extremachange",function(b){a._config.onExtremaChange&&a._config.onExtremaChange({min:b.min,max:b.max,gradient:a._config["gradient"]||a._config["defaultGradient"]})});d.setCoordinator(c)};function g(){var g=this._config=f.merge(b,arguments[0]||{});this._coordinator=new a;if(g["plugin"]){var h=g["plugin"];if(!b.plugins[h]){throw new Error("Plugin '"+h+"' not found. Maybe it was not registered.")}else{var i=b.plugins[h];this._renderer=new i.renderer(g);this._store=new i.store(g)}}else{this._renderer=new e(g);this._store=new c(g)}d(this)}g.prototype={addData:function(){this._store.addData.apply(this._store,arguments);return this},removeData:function(){this._store.removeData&&this._store.removeData.apply(this._store,arguments);return this},setData:function(){this._store.setData.apply(this._store,arguments);return this},setDataMax:function(){this._store.setDataMax.apply(this._store,arguments);return this},setDataMin:function(){this._store.setDataMin.apply(this._store,arguments);return this},configure:function(a){this._config=f.merge(this._config,a);this._renderer.updateConfig(this._config);this._coordinator.emit("renderall",this._store._getInternalData());return this},repaint:function(){this._coordinator.emit("renderall",this._store._getInternalData());return this},getData:function(){return this._store.getData()},getDataURL:function(){return this._renderer.getDataURL()},getValueAt:function(a){if(this._store.getValueAt){return this._store.getValueAt(a)}else if(this._renderer.getValueAt){return this._renderer.getValueAt(a)}else{return null}}};return g}();var h={create:function(a){return new g(a)},register:function(a,c){b.plugins[a]=c}};a["h337"]=h})(this||window);;// Production steps of ECMA-262, Edition 5, 15.4.4.21
// Reference: http://es5.github.io/#x15.4.4.21
if (!Array.prototype.reduce) {
  Array.prototype.reduce = function(callback /*, initialValue*/) {
    'use strict';
    if (this == null) {
      throw new TypeError('Array.prototype.reduce called on null or undefined');
    }
    if (typeof callback !== 'function') {
      throw new TypeError(callback + ' is not a function');
    }
    var t = Object(this), len = t.length >>> 0, k = 0, value;
    if (arguments.length == 2) {
      value = arguments[1];
    } else {
      while (k < len && ! k in t) {
        k++; 
      }
      if (k >= len) {
        throw new TypeError('Reduce of empty array with no initial value');
      }
      value = t[k++];
    }
    for (; k < len; k++) {
      if (k in t) {
        value = callback(value, t[k], k, t);
      }
    }
    return value;
  };
}